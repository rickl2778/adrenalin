<?php

/**
 * @file
 * Contains Drupal\adrenalin\Form\MessagesForm.
 *
 * Create a simple Admin Form for the HEX value
 */

namespace Drupal\adrenalin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class FieldsForm extends ConfigFormBase {

    /**
     * @return array
     */
    protected function getEditableConfigNames() {
        return [
            'adrenalin.adminsettings',
        ];
    }

    /**
     * @return string
     */
    public function getFormId() {
        return 'adrenalin_form';
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     * @return array
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('adrenalin.adminsettings');
        $form['adrenalin_value'] = [
            '#type' => 'textfield',
            '#size' => 6,
            '#maxlength' => 6,
            '#title' => $this->t('Adrenalin HEX value'),
            '#description' => $this->t('HEX value to use as the background colour for the text field'),
            '#default_value' => strtoupper($config->get('adrenalin_value')),
        ];
        return parent::buildForm($form, $form_state);
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        parent::submitForm($form, $form_state);
        $this->config('adrenalin.adminsettings')
            ->set('adrenalin_value', $form_state->getValue('adrenalin_value'))
            ->save();

        /**
         * Flush the caches to load the new value and allow the page cache to be refreshed to use the new value
         *
         * As this can take a little time on a production site some sort of "waiting" spinner could be used to improve the UX
         */
        drupal_flush_all_caches();
    }
}