<?php

/**
 * Adrenalin
 *
 * Controller for Adrenalin
 *
 */

namespace Drupal\adrenalin\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class AdrenalinController
 *
 * @package Drupal\adrenalin\Controller
 */
class AdrenalinController extends ControllerBase {
    /**
     * Content function that calls the adrenalin theme and sends values to that theme
     *
     * @return array
     */
    public function content() {

        /**
         * Get the adrenalin.adminsettings from the config database table
         */
        $config = \Drupal::config('adrenalin.adminsettings');
        /**
         * Get the HEX value from the adminsettings
         */
        $value = strtoupper($config->get('adrenalin_value'));

        return [
            '#theme' => 'adrenalin',
            '#teststring' => $this->t('This is a test string'),
            '#value' => $value,
        ];

    }
}